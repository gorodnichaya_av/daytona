(function($){
  //main menu click
  var menuBtn      = $('.menu-icon'),
      menuClose    = $('.main-menu-overlay, .main-menu__close'),
      toTopBtn     = $('.top-btn'),
      windowHeight = $(window).height();
      headerHeight = $('.header').height() + 76;

  menuBtn.click(function(){
    var parent = $(this).closest('.header');

    parent.find('.main-menu').addClass('main-menu--visible');
    parent.find('.main-menu-overlay').fadeIn(500);
    $('body').addClass('overflow');
  });

  menuClose.click(function() {
    var parent = $(this).closest('.header');

    parent.find('.main-menu').removeClass('main-menu--visible');
    parent.find('.main-menu-overlay').fadeOut(500);
    $('body').removeClass('overflow');
  });

  $(window).scroll(function () {
     if ($(this).scrollTop() > windowHeight) {
        toTopBtn.fadeIn();
     } else {
        toTopBtn.fadeOut();
     }
     if($(window).scrollTop() + $(window).height() >= $(document).height()){
      toTopBtn.fadeIn();
     }
     if ($(this).scrollTop() > headerHeight) {
        $('.header').addClass('header--small');
     } else {
        $('.header').removeClass('header--small');
     }
     if ($(this).scrollTop() > windowHeight/3) {
        $('.header__logo--home').addClass('header__logo--visible');
        $('.home__logo').addClass('home__logo--hidden');
     } else {
       $('.header__logo--home').removeClass('header__logo--visible');
        $('.home__logo').removeClass('home__logo--hidden');
     }
   });

   toTopBtn.click(function () {
     $("html, body").animate({
         scrollTop: 0
     }, 600);
     return false;
   });

   //choosing language on mobile
   $( "#language-select" ).on('change', (function() {
    window.location.href = $(this).val();
  }));

})(jQuery);
