(function($){
  $('.objects-panel__link--active').prev().addClass('objects-panel__link--no-padding-right');

  $('.objects-panel__mobile-btn').click(function(){
    var dropList = $(this).closest('.objects-panel').find('.objects-panel__items');
    if(dropList.hasClass('opened')) {
      dropList.removeClass('opened').slideUp();
    } else {
      dropList.addClass('opened').slideDown();
    }
  });
})(jQuery);

// slider for objects
$().fancybox({
  selector : '.slick-slide:not(.slick-cloned)',
  hash     : false,
  infobar  : false,
  thumbs: {
    autoStart : true,
    axis     : 'x',
  },
});

$(".object__slider").slick({
  slidesToShow   : 1,
  slidesToScroll : 1,
  infinite : true,
  dots: true,
});
