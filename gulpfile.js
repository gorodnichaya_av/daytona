var gulp         = require('gulp'),
    sass         = require("gulp-sass"),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin     = require('gulp-imagemin'),
    browserSync  = require('browser-sync');

gulp.task('browser-sync', function(){
  browserSync({
    server: {
      baseDir: './'
    },
    notify: false,
  })
});

gulp.task("sass", () =>
  gulp.src("src/scss/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("css"))
    .pipe(autoprefixer({
        browsers: ['last 7 versions'],
        cascade: false
    }))
    .pipe(gulp.dest("css"))
    .pipe(browserSync.reload({stream: true}))
);

gulp.task('imagemin', () =>
  gulp.src('img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('img'))
);

gulp.task('watch', ['browser-sync', 'sass'], function(){
  gulp.watch('src/scss/*/*.scss', ['sass']);
  gulp.watch('*.html', browserSync.reload);
});
